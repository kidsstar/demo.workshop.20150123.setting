﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Assembly.Demo {

    public class Fuga : MonoBehaviour {

        private Text uiText;

        public void Start() {
            uiText = this.gameObject.GetComponent<Text>();
        }

        public void FixedUpdate() {
            if (!string.IsNullOrEmpty(KidsStar.Setting.Hoge.instance.fuga)) {
                uiText.text = KidsStar.Setting.Hoge.instance.fuga;
            } else {
                uiText.text = "Unknown";
            }
        }

    }

}