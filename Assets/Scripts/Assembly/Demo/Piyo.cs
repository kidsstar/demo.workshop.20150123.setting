﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Assembly.Demo {

    public class Piyo : MonoBehaviour {

        private Text uiText;

        public void Start() {
            uiText = this.gameObject.GetComponent<Text>();
        }

        public void FixedUpdate() {
            if (-1 != KidsStar.Setting.Hoge.instance.piyo) {
                uiText.text = string.Format("{0}", KidsStar.Setting.Hoge.instance.piyo);
            } else {
                uiText.text = "Unknown";
            }
        }
    }

}