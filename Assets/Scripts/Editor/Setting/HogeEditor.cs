﻿using UnityEngine;
using UnityEditor;
using KidsStar.Setting;
using System.Collections;

namespace KidsStar.Setting {

    [CustomEditor(typeof(Hoge))]
    public class HogeEditor : Editor {
        private GUIContent labelFuga = new GUIContent("ふが [?]", "何か string な値");

        private GUIContent labelPiyo = new GUIContent("ぴよ [?]", "何か int な値");

        private Hoge instance;

        private bool showHogeSetting = true;

        public override void OnInspectorGUI() {
            instance = (Hoge)target;

            showHogeSetting = EditorGUILayout.Foldout(showHogeSetting, "ほげ設定");
            if (showHogeSetting) {
                EditorGUILayout.HelpBox("ほげに関する設定", MessageType.None);

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(labelFuga, GUILayout.Width(75));
                instance.fuga = EditorGUILayout.TextField(instance.fuga);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(labelPiyo, GUILayout.Width(75));
                instance.piyo = EditorGUILayout.IntSlider(instance.piyo, 0, 100);
                EditorGUILayout.EndHorizontal();
            }
        }

        [MenuItem("KidsStar/Setting/Hoge", false, 1)]
        public static void EditHogeSetting() {
            Selection.activeObject = KidsStar.Setting.Hoge.instance;
        }
    }
}