﻿using UnityEngine;
using System.IO;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace KidsStar.Setting {

    public abstract class Base : ScriptableObject {

        public const string SETTING_BASE_DIRECTORY = "Resources";

        public const string SETTING_DIRECTORY = "Settings";

        public const string SETTING_EXTENSION = ".asset";

        protected static void PrepareDirectory() {
#if UNITY_EDITOR
            // 再帰的にディレクトリ作れるようになると便利なのになぁ...
            if (!Directory.Exists(string.Format("Assets/{0}", SETTING_BASE_DIRECTORY))) {
                AssetDatabase.CreateFolder("Assets", SETTING_BASE_DIRECTORY);
            }
            if (!Directory.Exists(string.Format("Assets/{0}/{1}", SETTING_BASE_DIRECTORY, SETTING_DIRECTORY))) {
                AssetDatabase.CreateFolder(string.Format("Assets/{0}", SETTING_BASE_DIRECTORY), SETTING_DIRECTORY);
            }
#endif
        }

        protected static void CreateAsset<T>(T instance) where T : Base {
#if UNITY_EDITOR
            AssetDatabase.CreateAsset(instance, Path.Combine(Path.Combine("Assets", Path.Combine(SETTING_BASE_DIRECTORY, SETTING_DIRECTORY)), CreateFileName<T>()));
#endif
        }

        protected static string CreateFileName<T>() where T : Base {
            return CreateFileName<T>(true);
        }

        protected static string CreateFileName<T>(bool withExtension) where T : Base {
            if (withExtension) {
                return string.Format("{0}{1}", typeof(T).Name, SETTING_EXTENSION);
            }
            return typeof(T).Name;
        }

        protected static void SetDirty(Base instance) {
#if UNITY_EDITOR
            EditorUtility.SetDirty(instance);
#endif
        }

    }

}