﻿using UnityEngine;
using System.IO;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace KidsStar.Setting {

#if UNITY_EDITOR
    [InitializeOnLoad]
#endif
    public class Hoge : Base {

        [SerializeField]
        private string _fuga = string.Empty;

        public string fuga {
            get {
                return _fuga;
            }
            set {
                if (_fuga != value) {
                    _fuga = value;
                    SetDirty(this);
                }
            }
        }

        [SerializeField]
        private int _piyo = 0;

        public int piyo {
            get {
                return _piyo;
            }
            set {
                if (_piyo != value) {
                    _piyo = value;
                    SetDirty(this);
                }
            }
        }

        private static Hoge _instance;

        public static Hoge instance {
            get {
                if (null == _instance) {
                    _instance = Resources.Load(SETTING_DIRECTORY + "/" + CreateFileName<Hoge>(false)) as Hoge;
                    if (null == _instance) {
                        _instance = CreateInstance<Hoge>();
#if UNITY_EDITOR
                        PrepareDirectory();
                        CreateAsset<Hoge>(_instance);
#endif
                    }
                }
                return _instance;
            }
        }

    }
}
